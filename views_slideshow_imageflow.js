
/**
 *  @file
 *  This will initiate any ImageFlow browsers we have set up.
 */
Drupal.behaviors.viewsSlideshowImageFlow = function (context) {
  $('.views-slideshow-imageflow-images:not(.viewsSlideshowImageFlow-processed)', context).addClass('viewsSlideshowImageFlow-processed').each(function () {
    var imageflow = new ImageFlow();
    imageflow.init({ ImageFlowID: $(this).attr('id'), reflections: false });
  });
};
