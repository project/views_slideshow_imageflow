<?php

/**
 *  @file
 *  The default options available with Views Slideshow: ImageFlow.
 */

function views_slideshow_imageflow_views_slideshow_modes() {
  $options = array(
    'imageflow' => t('ImageFlow'),
  );
  return $options;
}

function views_slideshow_imageflow_views_slideshow_option_definition() {
}

function views_slideshow_imageflow_views_slideshow_options_form(&$form, &$form_state, &$view) {
}
